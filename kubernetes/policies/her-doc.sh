vault policy write wordpress -<<EOF
path "auth/approle/login" {
  capabilities = ["create", "read"]
}

path "database/creds/mira-edu" {
  capabilities = ["read"]
}
EOF