#!/bin/bash

delete_secret_anterior(){
  kubectl delete secret wp-password wp-user
}

cleanup(){
  rm -rf creds.json
}

get_vault_pods(){
  VAULT_POD=$(kubectl get pods --namespace default -l "app=vault" -o jsonpath="{.items[0].metadata.name}")
}

get_cred(){
  kubectl exec -it ${VAULT_POD} -- vault read --format=json database/creds/mira-edu >> creds.json
}

get_user(){
  WORDPRESS_DB_USER=$(cat creds.json | jq '.data.username' | tr -d '"",' | cut -d: -f2-2)
}

get_pass(){
  WORDPRESS_DB_PASSWORD=$(cat creds.json | jq '.data.password' | tr -d '"",' | cut -d: -f2-2)
}

criar_secrets(){
  # delete_secret_anterior &&
  get_vault_pods
  get_cred &&
  get_user && 
  get_pass &&
  kubectl create secret generic wp-password --from-literal=password=${WORDPRESS_DB_PASSWORD} &&
  kubectl create secret generic wp-user --from-literal=username=${WORDPRESS_DB_USER}
  cleanup
}

criar_deploy_wp(){
  kubectl apply -f wordpress-deployments.yaml
}

deploy_wp(){
  criar_secrets &&
  criar_deploy_wp
}

deploy_wp