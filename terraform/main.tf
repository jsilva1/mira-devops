provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_instance" "miraedu" {
  ami                         = "${data.aws_ami.ubuntu.id}"
  instance_type               = "${var.instance_type}"
  user_data                   = "${file("${path.module}/user-data.sh")}"
  key_name                    = "${var.key_name}"
  security_groups             = "${var.security_groups}"
  associate_public_ip_address = true
  subnet_id                   = "${var.subnet_id}"
  volume_tags                 = "${var.tags}"
  tags                        = "${var.tags}"
}
