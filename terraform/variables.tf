
variable "region"  { 
  description = "Região da AWS por padrao está Sao Paulo"
  default = "sa-east-1" 
}

variable "access_key"  { 
  description = "Chave de acesso AWS"
  default = "" 
}

variable "secret_key"  { 
  description = "Secret Key de acesso AWS"
  default = "" 
}

variable "key_name"  { 
  description = "Key par para acesso as instancias da AWS"
  default = "" 
}

variable "instance_type" { 
  description = "Perfil da Instancia"
  default = "t2.small" 
}

variable "subnet_id" {
  description = "Rede AWS onde a instancia irá subir, neste caso tera de ser pubica"
  default = "subnet-2416fa42" # Dado Fake
}

variable "security_groups" {
  description = "Lista com os Ids dos SG que será atachado na Instancia"
  default = ["sg-007a310a4456ebfd9"] # Dado Fake
}

variable "tags" {
  default = {
    Jailson  = "App"
    Environment = "Dev"
    Name = "MiraDevops-Teste"
  }
}
