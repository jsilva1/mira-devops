#!/bin/bash
set -e
exec > >(tee /var/log/bootstrap.log|logger -t user-data -s 2>/dev/console) 2>&1
echo BEGIN

CHEF_ROLE="app.json"
TAG_NAME="Name"
AVAILABILITY_ZONE=$(ec2metadata --availability-zone)
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | grep region | awk -F\" '{print $4}')
INSTANCE_ID=$(ec2metadata --instance-id)
INSTANCE_NAME=$(aws ec2 describe-tags --region $REGION --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=$TAG_NAME" | grep Value | awk '{print $2}' | tr -d '"",' | cut -d: -f2-2)
DOMAIN=$(grep -e '^search ' /etc/resolv.conf | awk '{print $2}' | tail -n 1)
FQDN="$INSTANCE_NAME.$DOMAIN"
IP=$(ec2metadata --local-ipv4)

instala_chef_client(){
  curl -L https://omnitruck.chef.io/install.sh | sudo bash
}

configurar_hostname(){
  hostname $INSTANCE_NAME
  echo "$INSTANCE_NAME" > /etc/hostname
  echo "$IP $FQDN $INSTANCE_NAME" >> /etc/hosts
}

remover_diretorio(){
  rm -rf /opt/mira-devops
}

clonar_repositorio(){
  /usr/bin/git clone https://jsilva1@bitbucket.org/jsilva1/mira-devops.git /opt/mira-devops
}

executar_chef(){
  /usr/bin/chef-solo -c /opt/mira-devops/chef/solo.rb -j /opt/mira-devops/chef/${CHEF_ROLE}
}

fazer_deploy(){
  instala_chef_client
  configurar_hostname
  rm -rf /opt/mira-devops
  clonar_repositorio
  executar_chef
}

fazer_deploy

echo END
