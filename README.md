# Projeto MIRA Educação

Este projeto contém o manifestos para realizar o deploy de uma aplicacação (frontend/backend)containerzidado na AWS e no Kubernetes

## Tecnologias escolhidas

Para este teste eu escolher as tecnologias:

- Docker
- CHEF
- Hashicorp Vault
- Hashicorp Terraform
- Inspec
- ChefSpec

## Docker

Um dos requisitos do testes erá a criação de aplicações contêinerizada.

### Hashicopr Vault

Será responsável por criar as credenciais do bando de dados Mysql, estou subindo dois container, um server para guardar as credenciais e um cliente que irá acessar o server e criar as credenciais dinâmicas do Mysql.

### CHEF

Será o configuration management responsável por toda a instalação e configuração quando o deploy for realizado em uma EC2

No CHEF foram desenvolvidas os custom resources para executar todas as tarefas, os testes de integração estão na pasta /test/integration/default, já os testes unitários estão na pasta /spec.

Para executá-los basta ter instalados o Kitchen e o chefspec e executá-los.

### Terraform

Será responsável para subir um servidor e executar o CHEF na AWS de forma automática.

### Worpress

Escolhida aleatoriamente como aplicação de testes.

## Testar localmente

Para fazer o deploy localmente do Worpress que tem como requisito um banco de dados Mysql, podemos executar o script deploy-wp.sh

```bash
cd application
bash deploy-wp.sh
```

A aplicação ira subir na port 8080

<http://localhost:8080>

Este script sobe todo o backend (Mysql, Vault) onde o Vault será responsável por criar o usuário e senha do bando de dados do Wordpress, depois irá coletar as credenciais de usua2rio e senha do bando de dados no Vault e injetar como variáveis de ambiente no container do Wordpress.

## Testes na AWS

Requisitos:

Terraform

Para a AWS pode-se utilizar o [Terraform](https://www.terraform.io/), por padrão a região que o servidores está subindo é Sao Paulo, porém algumas informações devem ser passadas, são elas;

- access_key
- secret_key
- subnet_id
- security_groups

O Terraform irá subir um servidor com um IP público atachado, neste caso será necessário a subnet ser pública.

Com as informações fornecidas podemos iniciar o terraform

```bash
cd terraform \
terraform init
```

Rodar o plan para verificar o que será criado.

```bash
cd terraform \
terraform plan
```

Agora rodar o apply para subir o servidor:

```bash
cd terraform \
terraform apply
```

O Terraform iniciará o servidor e irá executar um user-data que por us vez instalara o chef-client e rodar a recipe miradevops que é responsável por subir todo o backend e o Worpress em container.

A Aplicação poderá ser acessado pelo endereço <http://IP_PUBLICO_EC2>

O IP pública será mostrado no output do Terraform

Os logs do user-data estão no arquivo:

```bash
/var/log/bootstrap.log
```

Disclaimer: Durante um dos testes a chave para instalação do Docker não foi encontrata, isto aconteceu apenas umas vez e não consegui reproduzir o erro, mas caso aconteça no testes na MIRA basta executar os commando

```bash
terraform destroy
```

E novamente o apply

```bash
terraform apply
```

Ou rodar o user-data.sh novamente

Caso não queira utilizar o Terraform basta rodar o scritp user-data.sh em qualquer instancia da AWS para subir o projeto.

```bash
bash user-data.sh
```

## Testar no k8s

Para testar no K8s, podemos primeiro subir todo o Backend.

```bash
kubectl apply -f mysql-server-deployment.yaml
```

Criar Usuario no Bando de dados

kubectl exec -it POD_ID_MYSQL_SERVER -- mysql -uroot -pmysql

Executar o código abaixo no container do Mysql

```sql
CREATE USER 'vault'@'%' IDENTIFIED BY 'vault';
GRANT ALL PRIVILEGES ON *.* TO 'vault'@'%' WITH GRANT OPTION;
GRANT CREATE USER ON *.* to 'vault'@'%';

# Importante Pressionar ENTER.
```

Sair do pod MysqlServer

Subir Vault Server

```bash
kubectl apply -f vault-server-deployment.yaml
```

Subir pod do Vault Client

```bash
kubectl apply -f vault-client-pod.yaml
```

Fazer deploy do Wordpress

```bash
bash deploy-wp.sh
```