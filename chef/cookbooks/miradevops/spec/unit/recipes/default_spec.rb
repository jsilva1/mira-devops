#
# Cookbook:: miradevops
# Spec:: default
#
# Copyright:: 2018, Jailson Silva
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'spec_helper'

describe 'miradevops::default' do
  context 'When all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      # for a complete list of available platforms and versions see:
      # https://github.com/customink/fauxhai/blob/master/PLATFORMS.md
      runner = ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '16.04')
      runner.converge(described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'instalacao do Docker' do
      expect(chef_run).to install_miradevops_install_docker('mira-docker')
    end

    it 'subir aplicacoes backend' do
      expect(chef_run).to deploy_miradevops_deploy_backend('mira-databases')
    end

    it 'subir aplicacoes frontend' do
      expect(chef_run).to deploy_miradevops_deploy_frontend('mira-app')
    end

    it 'Verificar se a credencial existe no Vault' do
      expect(chef_run).to run_miradevops_aguarda_vault('verificar_credencais')
    end
  end
end
