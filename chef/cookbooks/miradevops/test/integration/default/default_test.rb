# # encoding: utf-8

# Inspec test for recipe miradevops::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe package('docker-engine') do
  it { should be_installed }
end

%w(mysql-server vault-server).each do |container|
  describe docker_container(container) do
    it { should exist }
    it { should be_running }
  end
end