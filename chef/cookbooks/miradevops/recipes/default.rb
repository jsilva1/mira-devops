#
# Cookbook:: miradevops
# Recipe:: default
#
# Copyright:: 2018, Jailson Silva

miradevops_install_docker 'mira-docker' do
  action :install
end

miradevops_deploy_backend 'mira-databases' do
  action :deploy
end

miradevops_aguarda_vault 'verificar_credencais' do
  comando 'docker exec vault-server vault read database/creds/mira-edu'
  mensagem 'Credenciais no Vault nao encontradas'
  intervalo 10
  action :run
end

miradevops_deploy_frontend 'mira-app' do
  action :deploy
end
