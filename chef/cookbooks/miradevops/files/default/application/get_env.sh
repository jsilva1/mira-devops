#!/bin/bash

set -e
exec > >(tee /var/log/descriptografar_creds.log|logger -t descriptografar -s 2>/dev/console) 2>&1
echo BEGIN

cleanup(){
  rm -rf wordpress.env
}

get_infos(){
  VAULT_SETTINGS=$(docker exec vault-server vault read --format=json database/creds/mira-edu)
}

get_username(){
  echo WORDPRESS_DB_USER=$(echo $VAULT_SETTINGS | docker run --rm -i colstrom/jq -r '.data.username') >> wordpress.env
}

get_cred(){
  echo WORDPRESS_DB_PASSWORD=$(echo $VAULT_SETTINGS | docker run --rm -i colstrom/jq -r '.data.password') >> wordpress.env
}

mysql_host(){
  echo WORDPRESS_DB_HOST=$(echo mysql-server) >> wordpress.env
}

descriptografar(){
  cleanup &&
  aguardar_vault &&
  get_infos &&
  get_username &&
  get_cred &&
  mysql_host &&
}

descriptografar

echo BEGIN