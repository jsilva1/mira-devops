# To learn more about Custom Resources, see https://docs.chef.io/custom_resources.html

action :deploy do
  bash 'consultar Vault' do
    cwd node['miradevops']['app_dir']
    code <<-EOH
    #!/bin/bash
      cleanup(){
        rm -rf wordpress.env
      }
      get_infos(){
        VAULT_SETTINGS=$(docker-compose exec -T vault vault read --format=json database/creds/mira-edu)
      }
      get_username(){
        echo WORDPRESS_DB_USER=$(echo $VAULT_SETTINGS | docker run --rm -i colstrom/jq -r '.data.username') >> wordpress.env
      }
      get_cred(){
        echo WORDPRESS_DB_PASSWORD=$(echo $VAULT_SETTINGS | docker run --rm -i colstrom/jq -r '.data.password') >> wordpress.env
      }
      mysql_host(){
        echo WORDPRESS_DB_HOST=$(echo mysql-server) >> wordpress.env
      }
      cleanup && get_infos && get_username && get_cred && mysql_host
      EOH
  end

  execute 'remove confainer anteriores' do
    command "docker rm -f #{node['miradevops']['app_name']}"
    action :run
    only_if "docker ps -a | grep #{node['miradevops']['app_name']}"
  end

  execute 'subir aplicacap' do
    command "docker run --name=#{node['miradevops']['app_name']} -d -p 80:80 --env-file wordpress.env --network app_default --link mysql-server wordpress:#{node['miradevops']['versao_app']}"
    cwd node['miradevops']['app_dir']
    action :run
  end
end
