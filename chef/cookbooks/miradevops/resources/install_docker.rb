# To learn more about Custom Resources, see https://docs.chef.io/custom_resources.html

action :install do
  execute 'adcionar chaves' do
    command 'sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D'
  end

  execute 'adicionar repo' do
    command "sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'"
  end

  execute 'instalar docker compose' do
    command 'sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && sudo chmod +x /usr/local/bin/docker-compose'
  end

  apt_update 'update'
  apt_package 'docker-engine'
end
