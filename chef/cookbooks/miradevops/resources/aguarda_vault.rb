
property :comando, String
property :mensagem, String
property :intervalo, Integer

action :run do
  command = new_resource.comando
  message = new_resource.mensagem
  wait_interval = new_resource.intervalo

  ruby_block new_resource.name do
    block do
      until system(command) do
        Chef::Log.info message
        sleep wait_interval
      end
    end
  end
  new_resource.updated_by_last_action(true)
end