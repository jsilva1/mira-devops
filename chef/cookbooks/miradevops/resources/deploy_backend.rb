# To learn more about Custom Resources, see https://docs.chef.io/custom_resources.html

action :deploy do
  remote_directory node['miradevops']['app_dir'] do
    source 'application'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
  end

  execute 'subir_backend' do
    command 'docker-compose up -d'
    cwd node['miradevops']['app_dir']
    action :run
  end
end
