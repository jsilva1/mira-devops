if defined?(ChefSpec)
  def miradevops_install_docker(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:miradevops_install_docker, :install, resource_name)
  end

  def miradevops_deploy_backend(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:miradevops_deploy_backend, :deploy, resource_name)
  end

  def miradevops_deploy_frontend(resource_name)
    ChefSpec::Matchers::ResourceMatcher.new(:miradevops_deploy_frontend, :deploy, resource_name)
  end
end
