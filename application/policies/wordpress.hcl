path "auth/approle/login" {
  capabilities = ["create", "read"]
}

path "database/creds/mira-edu" {
  capabilities = ["read"]
}