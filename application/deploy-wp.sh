#!/bin/bash

cleanup(){
  rm -rf wordpress.env
}

deploy_backend(){
  echo "Iniciando Backend ..."
  docker-compose up -d
  sleep 60
  echo "Backend Finalizado.."
}

get_infos(){
  echo "Iniciando Setup"
  VAULT_SETTINGS=$(docker-compose exec vault vault read --format=json database/creds/mira-edu)
}

get_username(){
  echo WORDPRESS_DB_USER=$(echo $VAULT_SETTINGS | docker run --rm -i colstrom/jq -r '.data.username') >> wordpress.env
}

get_cred(){
  echo WORDPRESS_DB_PASSWORD=$(echo $VAULT_SETTINGS | docker run --rm -i colstrom/jq -r '.data.password') >> wordpress.env
}

mysql_host(){
  echo WORDPRESS_DB_HOST=$(echo mysql-server) >> wordpress.env
  echo "Setup Finalizado ...."
}

run_wp_container(){
  echo "Iniciando Frontend ..."
  docker run --name=mira-wp -d -p 8080:80 --env-file wordpress.env --network application_default --link mysql-server wordpress:4.9.3
}

deploy_wordpress(){
  cleanup &&
  deploy_backend &&
  get_infos &&
  get_username &&
  get_cred &&
  mysql_host &&
  run_wp_container &&
  cleanup
  echo "Deploy finalizado, aplicação pode ser acessada pelo endereço http://localhost:8080"
}

deploy_wordpress